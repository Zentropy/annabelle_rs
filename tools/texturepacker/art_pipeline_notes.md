In order to make this work for arbitrary internet spritesheet downloads:
1) Open TexturePacker > Settings and change custom exporter directory to the exporters subdir next to this document
2) Unpack the downloaded spritesheet with Alferd's Spritesheet Unpacker by selecting all and exporting
3) Dump those pngs into TexturePacker
4) Choose Bevy Data Format in the dropdown
5) Change Algorithm from MaxRects to Grid
6) Export
7) Feed that to create_animation (see player.rs)
