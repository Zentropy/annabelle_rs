use crate::prelude::*;

/// Translation component that allows
/// egui to modify the collider bundle
#[derive(Component, Reflect)]
pub struct EguiColliderReflect {
    pub radius: f32,
    // Used for Ball and Capsule
    pub half_height: f32,
    // Used for Capsule
    pub size_x: f32,
    // Used for Cuboid
    pub size_y: f32,
    // Used for Cuboid
    pub offset_x: f32,
    // Collider offset relative to parent (sprite)
    pub offset_y: f32, // Collider offset relative to parent (sprite)

    pub collider_shape: ColliderShapeType, // for matching later in reflect_collider_egui
}

impl Default for EguiColliderReflect {
    fn default() -> Self {
        Self {
            radius: 30.0,
            half_height: 10.0,
            size_x: 20.0,
            size_y: 20.0,
            offset_x: 0.0,
            offset_y: 0.0,
            collider_shape: Default::default(),
        }
    }
}


/// Any time an EGUI change is made, apply it to the entity
pub fn reflect_collider_egui(
    mut commands: Commands,
    mut find_child_query: Query<(&Children), With<PlayerMarker>>,
    mut collider_reflect_query: Query<
        (&EguiColliderReflect),
        (With<PlayerMarker>, Changed<EguiColliderReflect>)
    >,
) {
    //if our collider_reflect comp hasn't changed, stop processing
    if collider_reflect_query.is_empty() {
        return;
    }
    dbg!("Updating collider because of change detected");

    let children = find_child_query.get_single().unwrap();
    let collider_child = children[0];

    for (reflect) in collider_reflect_query.iter_mut() {
        let collider = match reflect.collider_shape {
            ColliderShapeType::BALL => Collider::ball(reflect.radius),
            ColliderShapeType::CUBOID => Collider::cuboid(reflect.size_x, reflect.size_y),
            ColliderShapeType::CAPSULE => Collider::capsule_y(reflect.half_height, reflect.radius),
        };

        update_collider_entity(&mut commands, collider_child, reflect, collider);
    }
}

/// Internal fn used to wipe out the entity's old collider/transform and update with new ones
/// Attempting to change these rather than add/removing them won't work because bevy_rapier is trash
fn update_collider_entity(commands: &mut Commands, collider_child: Entity, reflect: &EguiColliderReflect, new_collider: Collider) {
    let new_transform = Transform::from_xyz(reflect.offset_x * -1., reflect.offset_y, 0.);
    commands
        .entity(collider_child)
        // THIS ORDERING IS REQUIRED FOR DEBUG
        // THE DEBUG VIS OTHERWISE WONT UPDATE BASED ON TRANSFORM
        // remove/insert Collider always comes LAST!!!!!!!!!
        .remove::<TransformBundle>()
        .insert(TransformBundle::from_transform(new_transform))
        .remove::<Collider>()
        .insert(new_collider);
}
