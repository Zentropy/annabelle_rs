use std::fs::File;
use std::io::Read;

use serde::Deserialize;
use strum_macros::Display;

use crate::prelude::*;

#[derive(Display, Debug)]
pub enum Animations {
    IDLE,
    WALK,
}

#[derive(Clone, Component, Debug)]
pub struct Animation {
    pub anim_speed: f32,
    pub start: usize,
    pub end: usize,
    pub looping: bool,
    pub texture_atlas_handle: Handle<TextureAtlas>,
}

#[derive(Clone, Component, Debug)]
pub struct Animator {
    pub animation_map: AnimMap,
    pub current_animation: String,
    pub last_animation: String,
    pub timer: f32,
}

impl Animator {
    pub fn new(animation_map: AnimMap, initial_animation: Animations) -> Self {
        Self {
            animation_map,
            current_animation: initial_animation.to_string(),
            last_animation: initial_animation.to_string(),
            timer: 0.0,
        }
    }
}

pub fn create_animation(
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
    animation_path: &str,
    spawn_transform: Option<Transform>,
    animation_map: &mut AnimMap,
    animation_name: Animations,
    anim_speed_mult: f32,
) -> (SpriteSheetBundle, AnimationTimer) {
    let sprite_config = load_animation_config(animation_path);

    let texture_atlas_handle = create_new_texture_atlas(
        asset_server,
        texture_atlases,
        animation_path,
        &sprite_config,
    );
    // Use only the subset of sprites in the sheet that make up the animation -- For us, all of them!
    let animation_indices = AnimationIndices {
        first: 0,
        last: sprite_config.sprite_count - 1,
    };

    // Create default transform if caller didn't include one
    let transform = match spawn_transform {
        None => Transform::from_scale(Vec3::splat(1.)),
        Some(tf) => tf,
    };

    // Generic speed fudge
    let anim_speed = sprite_config.sprite_count as f32 / (60. * anim_speed_mult);

    let animation = Animation {
        anim_speed,
        start: 1,
        end: sprite_config.sprite_count,
        looping: sprite_config.looping,
        texture_atlas_handle: texture_atlas_handle.clone(),
    };

    // put the handle into animation map so we can fish it out later to swap them
    animation_map.insert(animation_name.to_string(), animation);

    let ssb = SpriteSheetBundle {
        texture_atlas: texture_atlas_handle,
        sprite: TextureAtlasSprite::new(animation_indices.first),
        transform,
        ..default()
    };
    let timer = AnimationTimer(Timer::from_seconds(0.5, TimerMode::Repeating));

    (ssb, timer)
}

pub fn animate_sprite(
    time: Res<Time>,
    mut query: Query<(
        &mut Animator,
        &mut TextureAtlasSprite,
        &mut Handle<TextureAtlas>,
    )>,
) {
    for (mut animator, mut sprite, mut texture_atlas) in query.iter_mut() {
        let current_anim = animator.current_animation.as_str();

        let anim = animator.animation_map[current_anim].clone();

        *texture_atlas = anim.texture_atlas_handle.clone();

        if animator.last_animation != animator.current_animation {
            sprite.index = anim.start - 1;
        }
        let delta_secs = time.delta().as_secs_f32();

        let timer = animator.timer;

        animator.timer = timer - delta_secs;
        if animator.timer <= 0. {
            animator.timer = anim.anim_speed;
            if anim.looping == true {
                sprite.index = ((sprite.index + 1 - (anim.start - 1))
                    % (anim.end - anim.start + 1))
                    + anim.start
                    - 1;
            } else {
                sprite.index += 1;
                if sprite.index > anim.end - 1 {
                    sprite.index = anim.end - 1;
                }
            }
        }
        animator.last_animation = animator.current_animation.clone();
    }
}

#[derive(Debug, Component)]
pub struct AnimationIndices {
    pub first: usize,
    pub last: usize,
}

#[derive(Component, Deref, DerefMut)]
pub struct AnimationTimer(pub Timer);

#[derive(Debug, Deserialize)]
pub struct SpriteAnimConfig {
    pub texture_width: u32,
    pub texture_height: u32,
    pub looping: bool,
    pub sprite_count: usize,
    pub sprite_width: f32,
    pub sprite_height: f32,
}

fn create_new_texture_atlas(
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
    animation_path: &str,
    sprite_config: &SpriteAnimConfig,
) -> Handle<TextureAtlas> {
    let mut tex_path = animation_path.to_owned();
    tex_path.push_str(".png");
    let texture_handle = asset_server.load(tex_path);

    // Create our atlas from it -- I took these settings straight from TexturePacker setup
    let texture_atlas = TextureAtlas::from_grid(
        texture_handle,
        Vec2::new(sprite_config.sprite_width, sprite_config.sprite_height),
        sprite_config.sprite_count,
        1,
        None,
        None,
    );
    let texture_atlas_handle = texture_atlases.add(texture_atlas);
    texture_atlas_handle
}

fn load_animation_config(animation_path: &str) -> SpriteAnimConfig {
    let mut ron_path = animation_path.to_owned();
    ron_path.push_str(".ron");
    let current_dir = std::env::current_dir().expect("Failed to get current directory");

    // Append the relative path to the current directory.
    let file_path = current_dir.join("assets").join(ron_path);

    let mut file = File::open(&file_path).expect("Failed opening file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Failed reading file");

    let sprite_config: SpriteAnimConfig =
        ron::de::from_str(&contents).expect("Failed deserializing file");
    sprite_config
}
