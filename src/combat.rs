use std::process::Child;

use crate::prelude::*;

pub fn apply_damage(
    mut commands: Commands,
    mut damage_events: EventReader<DamageEvent>,
    mut parent_query: Query<(&mut Health)>,
    mut child_query: Query<(&Parent), With<Collider>>,
) {
    for dmg in damage_events.iter() {
        println!("Got dmg for: {:?}", dmg.entity_damaged);
        if let Ok((parent)) = child_query.get_mut(dmg.entity_damaged) {
            let mut parent_health = parent_query
                .get_mut(parent.get())
                .expect("Collider parent doesn't have health component");

            parent_health.hp -= dmg.hp;

            screen_print!("HP: {}", parent_health.hp);
            println!("HP: {} Entity {:?}", parent_health.hp, dmg.entity_damaged);
        } else {
            println!("FUCK");
        }
    }
}

pub fn process_deaths(mut commands: Commands, mut health_query: Query<(Entity, &Health)>) {
    for (entity, health) in health_query.iter() {
        //info!("Health is {:?}", health.hp);
        if health.hp <= 0 {
            commands.entity(entity).despawn_recursive();
        }
    }
}
