use bevy_debug_text_overlay::OverlayPlugin;
use bevy_inspector_egui::quick::WorldInspectorPlugin;

use prelude::*;

mod animation;
mod cursor;
mod player;
mod enemy;
mod physics;
mod projectile;
mod egui_reflect;
mod components;
mod combat;
mod events;

pub mod prelude {
    pub use bevy::prelude::*;
    pub use bevy_debug_text_overlay::screen_print;
    pub use bevy_rapier2d::prelude::*;

    pub use crate::animation::*;
    pub use crate::combat::*;
    pub use crate::components::*;
    pub use crate::cursor::*;
    pub use crate::egui_reflect::*;
    pub use crate::enemy::*;
    pub use crate::events::*;
    pub use crate::physics::*;
    pub use crate::player::*;
    pub use crate::projectile::*;
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest())) // prevents blurry sprites
        .add_plugin(WorldInspectorPlugin::new())
        .add_plugin(OverlayPlugin { font_size: 32.0, ..default() })
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(50.))
        .add_plugin(RapierDebugRenderPlugin::default())

        .add_event::<DamageEvent>()

        .register_type::<EguiColliderReflect>()
        .register_type::<ColliderShapeType>()
        .register_type::<Health>()

        .add_startup_system(setup_camera)
        .add_startup_system(setup_player)
        .add_startup_system(setup_enemy)

        .add_system(animate_sprite)
        .add_system(move_player)
        .add_system(reflect_collider_egui)
        .add_system(display_events)
        .add_system(apply_damage)
        .add_system(process_deaths)
        .add_system(display_contact_info)
        .add_system(display_intersection_info)

        .run();
}

pub fn animation_sys(
    time: Res<Time>,
    mut query: Query<(
        &AnimationIndices,
        &mut AnimationTimer,
        &mut TextureAtlasSprite,
    )>,
) {
    for (indices, mut timer, mut sprite) in &mut query {
        timer.tick(time.delta());
        if timer.just_finished() {
            sprite.index = if sprite.index == indices.last {
                indices.first
            } else {
                sprite.index + 1
            };
        }
    }
}

#[derive(Component)]
struct GameCamera;

fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}
