use std::collections::HashMap;

use crate::prelude::*;

#[derive(Component)]
pub struct EnemyMarker;

pub fn setup_enemy(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    // Load spritesheet into RAM
    let mut anim_map: AnimMap = HashMap::new();
    let mut entity_commands = commands.spawn(EnemyMarker);

    // hardcoded trial and error
    let mut anim_speed = 4.0;

    // default transform
    let spawn_transform = Some(Transform::from_xyz(100., 0., 0.));

    let walk = create_animation(
        &asset_server,
        &mut texture_atlases,
        "spritesheets/zombie/walk",
        spawn_transform,
        &mut anim_map,
        Animations::WALK,
        anim_speed,
    );

    anim_speed = 3.5;
    let idle = create_animation(
        &asset_server,
        &mut texture_atlases,
        "spritesheets/zombie/idle",
        spawn_transform,
        &mut anim_map,
        Animations::IDLE,
        anim_speed,
    );

    let animator = Animator::new(anim_map, Animations::IDLE);
    let membership: Group = Group::from_bits_truncate(0b0100);
    let collision_filter: Group = Group::from_bits_truncate(0b1000);

    entity_commands
        .insert(animator)
        .insert(walk)
        .insert(idle)
        .insert(RigidBody::KinematicPositionBased)
        .insert(Movement { speed: 100. })
        .insert(Health { hp: 2, armor: 1 })
    ;

    add_collision_bundle(&mut entity_commands, Transform::default(), membership, collision_filter)
}
