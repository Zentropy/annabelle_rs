use crate::prelude::*;

pub struct DamageEvent {
    pub hp: i32,
    pub entity_damaged: Entity,
}
