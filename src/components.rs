use crate::prelude::*;

#[derive(Component, Reflect)]
pub struct Health {
    pub hp: i32,
    pub armor: i32,
}

impl Default for Health {
    fn default() -> Self {
        Self {
            hp: 10,
            armor: 5,
        }
    }
}

#[derive(Component)]
pub struct Movement {
    pub speed: f32,
}
