use bevy::ecs::system::EntityCommands;

use crate::prelude::*;

#[derive(Reflect)]
pub enum ColliderShapeType {
    BALL,
    CUBOID,
    CAPSULE,
}

impl Default for ColliderShapeType {
    fn default() -> Self {
        ColliderShapeType::CAPSULE
    }
}

/* A system that displays the events. */
pub fn display_events(
    mut commands: Commands,
    mut collision_events: EventReader<CollisionEvent>,
    mut contact_force_events: EventReader<ContactForceEvent>,
    mut damage_event: EventWriter<DamageEvent>,
) {
    for collision_event in collision_events.iter() {
        println!("Received collision event: {:?}", collision_event);
        screen_print!("Received collision event: {:?}", collision_event);
        if let CollisionEvent::Started(ent1, ent2, flags) = collision_event {
            info!("Sending damage event");
            damage_event.send(DamageEvent {
                hp: 1,
                entity_damaged: ent1.clone(),
            });
            damage_event.send(DamageEvent {
                hp: 1,
                entity_damaged: ent2.clone(),
            });
        }
    }

    for contact_force_event in contact_force_events.iter() {
        println!("Received contact force event: {:?}", contact_force_event);
    }
}

pub fn display_contact_info(
    rapier_context: Res<RapierContext>,
    query: Query<(Entity, &PlayerMarker)>,
) {
    let (entity, player) = query.get_single().unwrap();

    /* Iterate through all the contact pairs involving a specific collider. */
    for contact_pair in rapier_context.contacts_with(entity) {
        let other_collider = if contact_pair.collider1() == entity {
            contact_pair.collider2()
        } else {
            contact_pair.collider1()
        };

        // Process the contact pair in a way similar to what we did in
        // the previous example.
        dbg!("FOUND COLLISION!");
    }
}

pub fn display_intersection_info(
    rapier_context: Res<RapierContext>,
    query: Query<(Entity, &PlayerMarker)>,
) {
    let (entity, player) = query.get_single().unwrap();

    /* Iterate through all the intersection pairs involving a specific collider. */
    for (collider1, collider2, intersecting) in rapier_context.intersections_with(entity) {
        if intersecting {
            println!(
                "The entities {:?} and {:?} have intersecting colliders!",
                collider1, collider2
            );
        }
    }
}

pub fn add_collision_bundle(entity_commands: &mut EntityCommands, tf: Transform, membership: Group, collision_filter: Group) {
    entity_commands
        .with_children(|children| {
            // This lets us offset the collider position vs the sprite itself, which isn't centered
            children
                .spawn(Collider::capsule_y(64. / 2., 8.))
                .insert(ActiveCollisionTypes::all())
                .insert(ActiveEvents::all())
                .insert(CollisionGroups::new(membership, collision_filter))
                .insert(TransformBundle::from_transform(tf));
        });
}
