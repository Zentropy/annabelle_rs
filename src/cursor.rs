use crate::prelude::*;

#[derive(Resource)]
pub struct CursorOffset
{
    pub x:f32,
    pub y:f32,
}
