use std::collections::HashMap;
use std::default;
use std::string::ToString;

use bevy::ecs::system::EntityCommands;

use crate::prelude::*;

#[derive(Component)]
pub struct PlayerMarker;

pub type AnimMap = HashMap<String, Animation>;

pub fn setup_player(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    // Load spritesheet into RAM
    let mut anim_map: AnimMap = HashMap::new();
    let mut entity_commands = commands.spawn(PlayerMarker);

    // hardcoded trial and error
    let mut anim_speed = 4.0;

    // default transform
    let spawn_transform = Some(Transform::from_scale(Vec3::splat(1.0)));

    let walk = create_animation(
        &asset_server,
        &mut texture_atlases,
        "spritesheets/zombie/walk",
        spawn_transform,
        &mut anim_map,
        Animations::WALK,
        anim_speed,
    );

    anim_speed = 3.5;
    let idle = create_animation(
        &asset_server,
        &mut texture_atlases,
        "spritesheets/zombie/idle",
        spawn_transform,
        &mut anim_map,
        Animations::IDLE,
        anim_speed,
    );

    let animator = Animator::new(anim_map, Animations::IDLE);
    let offset: Vec3 = Vec3::new(-8., 0., 0.);
    let tf = Transform::from_translation(offset);

    //player can collide with enemy but not another player
    let membership: Group = Group::from_bits_truncate(0b1000);
    let collision_filter: Group = Group::from_bits_truncate(0b0100);

    //Player-specific components
    entity_commands
        .insert(animator)
        .insert(walk)
        .insert(Movement { speed: 100. })
        .insert(Health { hp: 22, armor: 12 })
        .insert(idle)
        .insert(RigidBody::KinematicPositionBased)
        .insert(EguiColliderReflect {
            radius: 64. / 2.0,
            half_height: 15.,
            collider_shape: ColliderShapeType::CAPSULE,
            ..Default::default()
        });

    add_collision_bundle(&mut entity_commands, tf, membership, collision_filter);
}

pub struct ActorCollisionBundle {}

pub fn move_player(
    time: Res<Time>,
    keys: Res<Input<KeyCode>>,
    mut query: Query<(&Movement, &mut Transform, &mut Animator), With<PlayerMarker>>,
) {
    for (player_movement, mut transform, mut animator) in query.iter_mut() {
        animator.current_animation = Animations::IDLE.to_string();

        if keys.pressed(KeyCode::W) {
            animator.current_animation = Animations::WALK.to_string();
            transform.translation.y += player_movement.speed * time.delta_seconds();
        }
        if keys.pressed(KeyCode::A) {
            transform.rotation = Quat::from_rotation_y(std::f32::consts::PI);
            animator.current_animation = Animations::WALK.to_string();
            transform.translation.x -= player_movement.speed * time.delta_seconds();
        }
        if keys.pressed(KeyCode::S) {
            animator.current_animation = Animations::WALK.to_string();
            transform.translation.y -= player_movement.speed * time.delta_seconds();
        }
        if keys.pressed(KeyCode::D) {
            transform.rotation = Quat::default();
            animator.current_animation = Animations::WALK.to_string();
            transform.translation.x += player_movement.speed * time.delta_seconds();
        }

        screen_print!("Current animation is: {}", animator.current_animation.to_string());
    }
}
